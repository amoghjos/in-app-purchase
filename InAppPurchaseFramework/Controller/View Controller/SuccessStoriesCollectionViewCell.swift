//
//  SuccessStoriesCollectionViewCell.swift
//  InAppPurchaseFramework
//
//  Created by SHIVANI DUBEY on 02/08/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

class SuccessStoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var successTitle: UILabel!
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var rateImageView: UIImageView!
    @IBOutlet weak var successDescription: UILabel!
    @IBOutlet weak var successBackgroundView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        successBackgroundView.layer.cornerRadius = 10
    }

}
