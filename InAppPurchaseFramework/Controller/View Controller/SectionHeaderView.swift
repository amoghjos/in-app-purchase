//
//  SectionHeaderView.swift
//  InAppPurchaseFramework
//
//  Created by SHIVANI DUBEY on 01/08/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

class SectionHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var sectionMainTitle: UILabel!
    @IBOutlet weak var sectionSubTitle: UILabel!
}
