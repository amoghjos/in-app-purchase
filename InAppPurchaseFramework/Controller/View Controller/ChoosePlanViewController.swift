//
//  ChoosePlanViewController.swift
//  InAppPurchaseFramework
//
//  Created by SHIVANI DUBEY on 10/08/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

public protocol ChoosePlanActionDelegate {
    func planChoosed(index: Int, discount: Bool)
}

class ChoosePlanViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var bottomConstrainst: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstrainst: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var subscribeButton: UIButton!
    @IBOutlet weak var plansTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    
    var privacyLink : String = ""
    var termsLink : String = ""
    var iapPrices : [Double] = []
    var iapDiscountPrices : [Double] = []
    var discount : Bool = false
    var choosePlanActionDelegate : ChoosePlanActionDelegate?
    var cellSelectedIndex : Int = 0
    var priceCurrency = "$"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeButton.layer.cornerRadius = 35
        let bundle = Bundle(for: type(of: self))
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            let widthOfScreen = UIScreen.main.bounds.size.width
            let heightOfScreen = UIScreen.main.bounds.size.height
            let lrMarginConstraint = (widthOfScreen - 500) / 2
            let tbMarginConstraint = (heightOfScreen - 650) / 2
            topConstraint.constant = tbMarginConstraint
            bottomConstrainst.constant = tbMarginConstraint
            trailingConstrainst.constant = lrMarginConstraint
            leadingConstraint.constant = lrMarginConstraint
            backButton.setImage(UIImage(named: K.General.closeImage, in: bundle, compatibleWith: nil), for: .normal)
        default:
            topConstraint.constant = 0
            bottomConstrainst.constant = 0
            trailingConstrainst.constant = 0
            leadingConstraint.constant = 0
            backButton.setImage(UIImage(named: K.General.closeImage, in: bundle, compatibleWith: nil), for: .normal)
        }
        
        plansTableView.delegate = self
        plansTableView.dataSource = self
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        self.plansTableView.contentInset = insets
        plansTableView.register(UINib(nibName: K.CellIdentifiers.planCell, bundle: bundle), forCellReuseIdentifier: K.CellIdentifiers.planCell)
        let indexPath = IndexPath(row: 0, section: 0)
        plansTableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
        let detailsName = K.General.plansDetail[indexPath.row][0] as? String ?? ""
        subscribeButton.setTitle("\(K.General.subscribeButton)\(detailsName)", for: .normal)
    }
    
    @IBAction func subscribeButtonClicked(_ sender: Any) {
        choosePlanActionDelegate?.planChoosed(index: cellSelectedIndex, discount: discount)
    }
    @IBAction func backButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func termsAndPrivacyClicked(_ sender: UIButton) {
        guard let termsOfServiceUrl = URL(string: termsLink) else {
            return
        }
        
        var url = termsOfServiceUrl
        if sender.tag == 1 { //Privacy Policy
            guard let privacyUrl = URL(string: privacyLink) else {
                return
            }
            url = privacyUrl
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: K.CellIdentifiers.planCell, for: indexPath) as? PlanTableViewCell else { return UITableViewCell() }
        return configurePlanCell(cell, index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsName = K.General.plansDetail[indexPath.row][0] as? String ?? ""
        cellSelectedIndex = indexPath.row
        subscribeButton.setTitle("\(K.General.subscribeButton)\(detailsName)", for: .normal)
    }
    
    func configurePlanCell(_ cell: PlanTableViewCell, index: Int) -> PlanTableViewCell {
        cell.recommendedBackgroundView.layer.cornerRadius = 5
        cell.planBackgroundView.layer.cornerRadius = 10
        cell.planBackgroundView.layer.borderWidth = 2
        if index == 0 {
            cell.recommendedHeight.constant = 40
            cell.recommendedBackgroundView.isHidden = false
        } else {
            cell.recommendedHeight.constant = 0
            cell.recommendedBackgroundView.isHidden = true
        }
        let details = K.General.plansDetail[index]
        cell.planNameLable.text = details[0] as? String ?? ""
        cell.planMonthsLable.text = details[1] as? String ?? ""
        if iapPrices.count == 3 {
            var price = iapPrices[index]
            let months = details[2] as? Int ?? 1
            let pricePerMonth = price / Double(months)
            cell.priceLable.text = String(format: "%@%.2f", priceCurrency, price)
            cell.perMonthPriceLable.text = String(format: "%@%.2f%@",priceCurrency, pricePerMonth, K.General.perMonth)
            if discount {
                var perVariable = ""
                if index == 0 {
                    perVariable = K.General.perYear
                } else if index == 1 {
                    perVariable = K.General.perQuarter
                } else {
                    perVariable = K.General.perMonth
                }
                let attrString = NSAttributedString(string: String(format: "%@%.2f%@", priceCurrency, price, perVariable), attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
                cell.perMonthPriceLable.attributedText = attrString
                price = iapDiscountPrices[index]
                cell.priceLable.text = String(format: "%@%.2f", priceCurrency, price)
            }
        } else {
            cell.priceLable.text = "0"
            cell.perMonthPriceLable.text = "0"
        }
        return cell
    }
}

extension ChoosePlanViewController : UpdatePlanDelegate {
    func updatePlan(iapPrices: [Double], iapDiscountedPrices: [Double], isDiscount: Bool) {
        self.iapPrices = iapPrices
        self.iapDiscountPrices = iapDiscountedPrices
        self.discount = isDiscount
        self.plansTableView.reloadData()
    }
    func closeView() {
        self.dismiss(animated: true, completion: nil)
    }
}
