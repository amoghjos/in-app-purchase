//
//  CollectionTableViewCell.swift
//  InAppPurchaseFramework
//
//  Created by SHIVANI DUBEY on 02/08/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

class CollectionTableViewCell: UITableViewCell {
    
    var successStories: [[String: Any]] = []
    var mainVC: IAPViewController?
    @IBOutlet weak var successStoriesCollection: UICollectionView!
    private var indexOfCellBeforeDragging = 0
    let cellWidth : CGFloat = 345
    let cellHeight : CGFloat = 260
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let bundle = Bundle(for: type(of: self))
        self.successStoriesCollection.register(UINib(nibName: K.CellIdentifiers.successCollectionCell, bundle: bundle), forCellWithReuseIdentifier: K.CellIdentifiers.successCollectionCell)
        successStoriesCollection.delegate = self
        successStoriesCollection.dataSource = self
        //successStoriesCollection.isPagingEnabled = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension CollectionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return successStories.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: K.CellIdentifiers.successCollectionCell, for: indexPath) as? SuccessStoriesCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.successTitle.text = successStories[indexPath.row][K.General.successComment] as? String ?? ""
        cell.successDescription.text = successStories[indexPath.row][K.General.successReview] as? String ?? ""
        cell.nameLable.text = successStories[indexPath.row][K.General.successName] as? String ?? ""
        cell.dateLable.text = successStories[indexPath.row][K.General.successDate] as? String ?? ""
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let bundle = Bundle(for: type(of: self))
        let successData = successStories[indexPath.row]
        let reviewView = RatingReviewViewController(nibName: K.CellIdentifiers.ratingReviewVC, bundle: bundle)
        reviewView.successData = successData
        reviewView.modalPresentationStyle = .fullScreen
        mainVC?.present(reviewView, animated: true, completion: nil)
    }
    
    private func indexOfMajorCell() -> Int {
        let proportionalOffset = successStoriesCollection.contentOffset.x / cellWidth
        let index = Int(round(proportionalOffset))
        let numberOfItems = successStories.count
        let safeIndex = max(0, min(numberOfItems - 1, index))
        print(safeIndex)
        return safeIndex
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // Stop scrollView sliding:
        targetContentOffset.pointee = scrollView.contentOffset

        // calculate where scrollView should snap to:
        let indexOfMajorCell = self.indexOfMajorCell()

        // calculate conditions:
        let dataSourceCount = collectionView(successStoriesCollection, numberOfItemsInSection: 0)
        let swipeVelocityThreshold: CGFloat = 0.5 // after some trail and error
        let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < dataSourceCount && velocity.x > swipeVelocityThreshold
        let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
        let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
        let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)

        if didUseSwipeToSkipCell {

            let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
            let toValue = cellWidth * CGFloat(snapToIndex)

            // Damping equal 1 => no oscillations => decay animation:
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: velocity.x, options: .allowUserInteraction, animations: {
                scrollView.contentOffset = CGPoint(x: toValue, y: 0)
                scrollView.layoutIfNeeded()
            }, completion: nil)

        } else {
            // This is a much better way to scroll to a cell:
            let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
            successStoriesCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
}
