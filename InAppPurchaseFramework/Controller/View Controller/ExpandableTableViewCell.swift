//
//  ExpandableTableViewCell.swift
//  InAppPurchaseFramework
//
//  Created by SHIVANI DUBEY on 02/08/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

protocol FAQLinkOpening {
    func openLink(url : URL)
}

class ExpandableTableViewCell: UITableViewCell {
    
    @IBOutlet weak var faqView: UIView!
    @IBOutlet weak var expandableImageView: UIImageView!
    @IBOutlet weak var questionLable: UILabel!
    @IBOutlet weak var answerLable: UILabel!
    var answerText : String = ""
    var delegate : FAQLinkOpening?

    override func awakeFromNib() {
        super.awakeFromNib()
        faqView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleURLClicked))
        answerLable.addGestureRecognizer(tap)
        answerLable.isUserInteractionEnabled = true
    }
    @objc func handleURLClicked(gesture: UITapGestureRecognizer) {
        
        for (ind, urlString) in answerText.detectedLinks.enumerated() {
            let stringURL = urlString as NSString
            let stringRange = stringURL.range(of: answerText)
            let tapLocation = gesture.location(in: answerLable)
            let index = answerLable.indexOfAttributedTextCharacterAtPoint(point: tapLocation)
            if checkRange(stringRange, contain: index) == true {
                let url = answerText.detectedURLs[ind]
                delegate?.openLink(url: url)
                return
            } else if answerText.detectedURLs.count == 1 {
                let url = answerText.detectedURLs[0]
                delegate?.openLink(url: url)
                return
            }
        }
    }
    
    func checkRange(_ range: NSRange, contain index: Int) -> Bool {
        return index > range.location && index < range.location + range.length
    }
}
