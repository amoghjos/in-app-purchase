//
//  ComparisonTableViewCell.swift
//  InAppPurchaseFramework
//
//  Created by SHIVANI DUBEY on 18/07/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

class ComparisonTableViewCell: UITableViewCell {
    @IBOutlet weak var isFreeImageView: UIImageView!
    @IBOutlet weak var isProImageView: UIImageView!
    @IBOutlet weak var isFreeLabel: UILabel!
    @IBOutlet weak var isProLabel: UILabel!
    @IBOutlet weak var title: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
