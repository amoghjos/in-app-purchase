//
//  RatingReviewViewController.swift
//  InAppPurchaseFramework
//
//  Created by SHIVANI DUBEY on 10/08/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

class RatingReviewViewController: UIViewController {
    
    @IBOutlet weak var successTitle: UILabel!
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var rateImageView: UIImageView!
    @IBOutlet weak var successDescription: UILabel!
    @IBOutlet weak var successBackgroundView: UIView!
    
    var successData: [String: Any] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        successBackgroundView.layer.cornerRadius = 10
        if successData.count > 0 {
            successTitle.text = successData[K.General.successComment] as? String ?? ""
            successDescription.text = successData[K.General.successReview] as? String ?? ""
            nameLable.text = successData[K.General.successName] as? String ?? ""
           dateLable.text = successData[K.General.successDate] as? String ?? ""
        }
    }
    
    @IBAction func goBack() {
        self.dismiss(animated: true, completion: nil)
    }


}
