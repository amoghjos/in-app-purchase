//
//  PlanTableViewCell.swift
//  InAppPurchaseFramework
//
//  Created by SHIVANI DUBEY on 12/08/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

class PlanTableViewCell: UITableViewCell {
    @IBOutlet weak var recommendedBackgroundView : UIView!
    @IBOutlet weak var planBackgroundView : UIView!
    @IBOutlet weak var planNameLable : UILabel!
    @IBOutlet weak var planMonthsLable : UILabel!
    @IBOutlet weak var priceLable : UILabel!
    @IBOutlet weak var perMonthPriceLable : UILabel!
    @IBOutlet weak var recommendedHeight : NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let bundle = Bundle(for: type(of: self))
        if selected {
            recommendedBackgroundView.backgroundColor = UIColor(named: K.General.recommendedColor, in: bundle, compatibleWith: nil)
            planBackgroundView.backgroundColor = UIColor(named: K.General.selectedCellBackgroundColor, in: bundle, compatibleWith: nil)
            if #available(iOS 13.0, *) {
                planBackgroundView.layer.borderColor = UIColor(named: K.General.recommendedColor, in: bundle, compatibleWith: nil)?.resolvedColor(with: self.traitCollection).cgColor
            } else {
                // Fallback on earlier versions
                 planBackgroundView.layer.borderColor = UIColor(named: K.General.recommendedColor, in: bundle, compatibleWith: nil)?.cgColor
            }
        }
        else {
            recommendedBackgroundView.backgroundColor = UIColor.black
            planBackgroundView.backgroundColor = UIColor(named: K.General.unselectedCellBackgroundColor, in: bundle, compatibleWith: nil)
            if #available(iOS 13.0, *) {
                planBackgroundView.layer.borderColor = UIColor(named: K.General.unselectedCellBackgroundColor, in: bundle, compatibleWith: nil)?.resolvedColor(with: self.traitCollection).cgColor
            } else {
                // Fallback on earlier versions
                planBackgroundView.layer.borderColor = UIColor(named: K.General.unselectedCellBackgroundColor, in: bundle, compatibleWith: nil)?.cgColor
            }
        }
    }
    
}
