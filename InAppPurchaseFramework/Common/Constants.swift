//
//  Constants.swift
//  InAppPurchaseFramework
//
//  Created by SHIVANI DUBEY on 01/08/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

struct K {
    struct HeaderNames {
        static let comparisonHeader: String = "Why to get Sleep Z Pro?"
        static let successHeader: String = "Success Stories"
        static let faqHeader: String = "Frequently Asked Questions"
        static let successSubHeader: String = "from the App Store"
        static let headerMainColor: String = "Header Section Main Title Color"
        static let headerSubColor: String = "Header Section Sub Title Color"
        static let headerProColor: String = "Header Section Pro Title Color"
    }
    struct CellIdentifiers {
        static let iapView : String = "IAPViewController"
        static let sectionHeaderView: String = "SectionHeaderView"
        static let comparisonCell: String = "ComparisonTableViewCell"
        static let comparisonEvenCellColor : String = "Even Comparison Cell Color"
        static let comparisonOddCellColor : String = "Odd Comparison Cell Color"
        static let comparisonEvenCellTextColor : String = "Even Comparison Cell Color"
        static let comparisonOddCellTextColor : String = "Odd Comparison Cell Color"
        static let collectionCell : String = "CollectionTableViewCell"
        static let expandableCell : String = "ExpandableTableViewCell"
        static let successCollectionCell : String = "SuccessStoriesCollectionViewCell"
        static let ratingReviewVC : String = "RatingReviewViewController"
        static let choosePlanVC : String = "ChoosePlanViewController"
        static let planCell : String = "PlanTableViewCell"
        static let expandImage : String = "Expand"
        static let collapseImage : String = "Collapse"
    }
    struct  General {
        static let featureTitle : String = "FeatureTitle"
        static let featureFree: String = "FeatureFree"
        static let featurePro: String = "FeaturePro"
        static let successComment: String = "Comment"
        static let successReview: String = "Review"
        static let successRate: String = "Rate"
        static let successName: String = "Name"
        static let successDate: String = "Date"
        static let question: String = "Question"
        static let answer: String = "Answer"
        static let expandable: String = "Expandable"
        static let isAvailableImage : String = "Check"
        static let isLockedImage: String = "Lock"
        static let OK: String = "OK"
        static let by: String = "by "
        static let on: String = "on "
        static let tableBGColor: String = "Header Background Color"
        static let recommendedColor: String = "Choose Plan Button Color"
        static let selectedCellBackgroundColor : String = "Plan Selected Color"
        static let unselectedCellBackgroundColor : String = "Plan Not Selected Color"
        static let plansDetail : [[Any]] = [["Yearly", "12 Months", 12], ["Quarterly", "3 Months", 3], ["Monthly", "1 Month", 1]]
        static let perMonth : String = "/month"
        static let perYear : String = "/year"
        static let perQuarter : String = "/quarter"
        static let subscribeButton : String = "Subscribe to Sleep Z Pro "
        static let closeImage : String = "Cross"
        static let backImage : String = "Back"
    }
    struct mainScreen {
        static let upgrade = "Upgrade"
        static let upgradeText1 = "Get 3x better sleep"
        static let upgradeText2 = "with Sleep Z Pro"
        static let limitedOffer = "Limited Time Offer"
        static let limitedOfferText1 = "FLAT 50% OFF"
        static let limitedOfferText2 = "Offer ends in "
    }
    struct AlertText {
        static let alertTitle = "Please connect to the internet to choose a Plan"
        static let actionTitle = "Ok"
    }
}

// MARK: DataDetector

class DataDetector {

    private class func _find(all type: NSTextCheckingResult.CheckingType,
                             in string: String, iterationClosure: (String) -> Bool) {
        guard let detector = try? NSDataDetector(types: type.rawValue) else { return }
        let range = NSRange(string.startIndex ..< string.endIndex, in: string)
        let matches = detector.matches(in: string, options: [], range: range)
        loop: for match in matches {
            for i in 0 ..< match.numberOfRanges {
                let nsrange = match.range(at: i)
                let startIndex = string.index(string.startIndex, offsetBy: nsrange.lowerBound)
                let endIndex = string.index(string.startIndex, offsetBy: nsrange.upperBound)
                let range = startIndex..<endIndex
                guard iterationClosure(String(string[range])) else { break loop }
            }
        }
    }

    class func find(all type: NSTextCheckingResult.CheckingType, in string: String) -> [String] {
        var results = [String]()
        _find(all: type, in: string) {
            results.append($0)
            return true
        }
        return results
    }

    class func first(type: NSTextCheckingResult.CheckingType, in string: String) -> String? {
        var result: String?
        _find(all: type, in: string) {
            result = $0
            return false
        }
        return result
    }
}

// MARK: String extension

extension String {
    var detectedLinks: [String] { DataDetector.find(all: .link, in: self) }
    var detectedFirstLink: String? { DataDetector.first(type: .link, in: self) }
    var detectedURLs: [URL] { detectedLinks.compactMap { URL(string: $0) } }
    var detectedFirstURL: URL? {
        guard let urlString = detectedFirstLink else { return nil }
        return URL(string: urlString)
    }
    static func format(strings: [String], boldColor: UIColor = UIColor.blue, inString string: String, font: UIFont = UIFont(name: "ABeeZee-Regular", size: 14.0) ?? UIFont.systemFont(ofSize: 14)) -> NSAttributedString {
        let attributedString =
            NSMutableAttributedString(string: string,
                                      attributes: [NSAttributedString.Key.font: font])
        
        for (x, bold) in strings.enumerated() {
            let boldFontAttribute = [NSAttributedString.Key.foregroundColor: boldColor, NSAttributedString.Key.link: string.detectedURLs[x]] as [NSAttributedString.Key : Any]
            attributedString.addAttributes(boldFontAttribute, range: (string as NSString).range(of: bold))
        }
        return attributedString
    }
}
// MARK: Label extension
extension UILabel {
    func indexOfAttributedTextCharacterAtPoint(point: CGPoint) -> Int {
        assert(self.attributedText != nil, "This method is developed for attributed string")
        let textStorage = NSTextStorage(attributedString: self.attributedText!)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: self.frame.size)
        textContainer.lineFragmentPadding = 0
        textContainer.maximumNumberOfLines = self.numberOfLines
        textContainer.lineBreakMode = self.lineBreakMode
        layoutManager.addTextContainer(textContainer)

        let index = layoutManager.characterIndex(for: point, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return index
    }
}
