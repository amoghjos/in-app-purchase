//
//  IAPViewController.swift
//  InAppPurchaseFramework
//
//  Created by SHIVANI DUBEY on 15/07/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

public protocol IAPScreenDelegate {
    func closeView()
    func iapValue(offerBool: Bool, offerTime: Date?)
    func appOpenCount(count: Int)
}

protocol UpdatePlanDelegate {
    func updatePlan(iapPrices: [Double], iapDiscountedPrices : [Double], isDiscount: Bool)
    func closeView()
}

public class IAPViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var choosePlanButton: UIButton!
    @IBOutlet weak var headerTitle : UILabel!
    @IBOutlet weak var headerTitleMsg1 : UILabel!
    @IBOutlet weak var headerTitleMsg2 : UILabel!
    
    public var faqDict: [[String: Any]] = [[:]]
    public var comparisonDict: [[String: Any]] = [[:]]
    public var successStories: [[String: Any]] = [[:]]
    public var iapDelegate: IAPScreenDelegate?
    public var choosePlanDelegate : ChoosePlanActionDelegate?
    var updatePlanDelegate : UpdatePlanDelegate?
    public var termsLink : String = ""
    public var privacyLink : String = ""
    var expandadedCell : Int = -1
    public var iapPrices : [Double] = []
    public var iapDiscountPrices : [Double] = []
    public var priceCurrency : String = "$"
    public var discount : Bool = false
    var timer : Timer?
    var offerTimer: Timer?
    public var offerTimeDate = Date()
    
    public init() {
        super.init(nibName: K.CellIdentifiers.iapView, bundle: Bundle(for: IAPViewController.self))
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.setHeader()
        closeButton.layer.cornerRadius = 20
        choosePlanButton.layer.cornerRadius = 35
    }
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func setHeader() {
        if discount {
            headerTitle.text = K.mainScreen.limitedOffer
            headerTitleMsg1.text = K.mainScreen.limitedOfferText1
            headerTitleMsg2.text = K.mainScreen.limitedOfferText2
            initOfferTimer()
        } else {
            headerTitle.text = K.mainScreen.upgrade
            headerTitleMsg1.text = K.mainScreen.upgradeText1
            headerTitleMsg2.text = K.mainScreen.upgradeText2
        }
    }
    
    func initOfferTimer() {
        offerTimer?.invalidate()
        offerTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateOfferTime), userInfo: nil, repeats: true)
    }
    @objc func updateOfferTime() {
        let seconds = Int(Date().timeIntervalSince(offerTimeDate))
        if seconds >= 86400 {
            offerTimer?.invalidate()
            iapDelegate?.iapValue(offerBool: false, offerTime: nil)
        }
        setOfferTime(seconds: seconds)
    }
    
    func setOfferTime(seconds: Int) {
        let (h, m, s) = secondsToHoursMinutesSeconds(seconds: seconds)
        let h_string = h < 10 ? "0\(h)" : "\(h)"
        let m_string =  m < 10 ? "0\(m)" : "\(m)"
        let s_string =  s < 10 ? "0\(s)" : "\(s)"
        headerTitleMsg2.text = String.init(format: "%@%@:%@:%@",K.mainScreen.limitedOfferText2, h_string, m_string, s_string)
    }
    
    public func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        let remainingSec = 86400 - seconds
        if remainingSec <= 0 {
            discount = false
            iapDelegate?.iapValue(offerBool: false, offerTime: nil)
            iapDelegate?.appOpenCount(count: 0)
            setHeader()
        }
        return (remainingSec / 3600, (remainingSec % 3600) / 60, (remainingSec % 3600) % 60)
    }
    
    //MARK: View Functions
    @IBAction func closeButton(_ sender: Any) {
        iapDelegate?.closeView()
    }
    
    @IBAction func choosePlanButton(_ sender: Any) {
        if NetworkReachability.isConnectedToNetwork() {
            let bundle = Bundle(for: type(of: self))
            let choosePlanVC = ChoosePlanViewController(nibName: K.CellIdentifiers.choosePlanVC, bundle: bundle)
            choosePlanVC.privacyLink = privacyLink
            choosePlanVC.termsLink = termsLink
            choosePlanVC.iapPrices = iapPrices
            choosePlanVC.iapDiscountPrices = iapDiscountPrices
            choosePlanVC.discount = discount
            self.updatePlanDelegate = choosePlanVC
            choosePlanVC.priceCurrency = priceCurrency
            choosePlanVC.choosePlanActionDelegate = choosePlanDelegate
            choosePlanVC.modalPresentationStyle = .overCurrentContext
            choosePlanVC.modalTransitionStyle = .crossDissolve
            self.present(choosePlanVC, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: K.AlertText.alertTitle, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: K.AlertText.actionTitle, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    public func updateIAPValue() {
        setHeader()
        updatePlanDelegate?.updatePlan(iapPrices: iapPrices, iapDiscountedPrices: iapDiscountPrices, isDiscount: discount)
    }
    public func closeViewOnSuccess() {
        updatePlanDelegate?.closeView()
    }
    
    //MARK: Table View datasource and delegate
    private func setupTableView() {
        let bundle = Bundle(for: type(of: self))
        tableView.tableHeaderView = self.headerView
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: 110, right: 0)
        self.tableView.contentInset = insets
        self.tableView.register(UINib(nibName: K.CellIdentifiers.sectionHeaderView, bundle: bundle), forHeaderFooterViewReuseIdentifier: K.CellIdentifiers.sectionHeaderView)
        self.tableView.register(UINib(nibName: K.CellIdentifiers.comparisonCell, bundle: bundle), forCellReuseIdentifier: K.CellIdentifiers.comparisonCell)
        self.tableView.register(UINib(nibName: K.CellIdentifiers.collectionCell, bundle: bundle), forCellReuseIdentifier: K.CellIdentifiers.collectionCell)
        self.tableView.register(UINib(nibName: K.CellIdentifiers.expandableCell, bundle: bundle), forCellReuseIdentifier: K.CellIdentifiers.expandableCell)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionFooterHeight = 0.0
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return comparisonDict.count // Comparision
        case 1:
            return 1 // Collection View for success stories
        default:
            return faqDict.count // FAQ
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.CellIdentifiers.comparisonCell, for: indexPath) as? ComparisonTableViewCell else {
                return UITableViewCell()
            }
            return configureComparisonCell(cell, index: indexPath.row)
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.CellIdentifiers.collectionCell, for: indexPath) as? CollectionTableViewCell else {
                return UITableViewCell()
            }
            return configureCollectionCell(cell)
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.CellIdentifiers.expandableCell, for: indexPath) as? ExpandableTableViewCell else {
                return UITableViewCell()
            }
            return configureFAQCell(cell, index: indexPath.row)
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let bundle = Bundle(for: type(of: self))
        guard let sectionView = tableView.dequeueReusableHeaderFooterView(withIdentifier: K.CellIdentifiers.sectionHeaderView) as? SectionHeaderView else {
            return nil
        }
        switch section {
        case 0:
            if (sectionView.sectionMainTitle != nil) {
                sectionView.sectionMainTitle.text = K.HeaderNames.comparisonHeader
                sectionView.sectionSubTitle.text = ""
                sectionView.sectionMainTitle.textColor = UIColor(named: K.HeaderNames.headerProColor, in: bundle, compatibleWith: nil)
            }
        case 1:
            if (sectionView.sectionMainTitle != nil) {
                sectionView.sectionMainTitle.text = K.HeaderNames.successHeader
                sectionView.sectionSubTitle.text = K.HeaderNames.successSubHeader
                sectionView.sectionMainTitle.textColor = UIColor(named: K.HeaderNames.headerMainColor, in: bundle, compatibleWith: nil)
                sectionView.sectionSubTitle.textColor = UIColor(named: K.HeaderNames.headerSubColor, in: bundle, compatibleWith: nil)
            }
        default:
            if (sectionView.sectionMainTitle != nil) {
                sectionView.sectionMainTitle.text = K.HeaderNames.faqHeader
                sectionView.sectionSubTitle.text = ""
                sectionView.sectionMainTitle.textColor = UIColor(named: K.HeaderNames.headerMainColor, in: bundle, compatibleWith: nil)
            }
        }
        return sectionView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 130
        default:
            return 90
        }
    }
    
    func configureComparisonCell(_ cell: ComparisonTableViewCell, index: Int) -> ComparisonTableViewCell {
        let bundle = Bundle(for: type(of: self))
        if index % 2 == 0 {
            if #available(iOS 12.0, *) {
                if self.traitCollection.userInterfaceStyle == .dark {
                    cell.backgroundColor = UIColor(named: K.CellIdentifiers.comparisonOddCellColor, in: bundle, compatibleWith: nil)
                } else {
                    cell.backgroundColor = UIColor(named: K.CellIdentifiers.comparisonEvenCellColor, in: bundle, compatibleWith: nil)
                }
            } else {
                cell.backgroundColor = UIColor(named: K.CellIdentifiers.comparisonEvenCellColor, in: bundle, compatibleWith: nil)
            }
            
        } else {
            if #available(iOS 12.0, *) {
                if self.traitCollection.userInterfaceStyle == .dark {
                    cell.backgroundColor = UIColor(named: K.CellIdentifiers.comparisonEvenCellColor, in: bundle, compatibleWith: nil)
                } else {
                    cell.backgroundColor = UIColor(named: K.CellIdentifiers.comparisonOddCellColor, in: bundle, compatibleWith: nil)
                }
            } else {
                cell.backgroundColor = UIColor(named: K.CellIdentifiers.comparisonOddCellColor, in: bundle, compatibleWith: nil)
            }
        }
        if index == 0 {
            cell.isProLabel.isHidden = false
            cell.isProImageView.isHidden = true
            cell.isFreeLabel.isHidden = false
            cell.isFreeImageView.isHidden = true
        } else {
            cell.isProLabel.isHidden = true
            cell.isProImageView.isHidden = false
            cell.isFreeLabel.isHidden = true
            cell.isFreeImageView.isHidden = false
            if let isFree = comparisonDict[index][K.General.featureFree] as? Bool, isFree == true {
                cell.isFreeImageView.image = UIImage(named: K.General.isAvailableImage, in: bundle, compatibleWith: nil)
            } else {
                cell.isFreeImageView.image = UIImage(named: K.General.isLockedImage, in: bundle, compatibleWith: nil)
            }
            if let isPro = comparisonDict[index][K.General.featurePro] as? Bool, isPro == true {
                cell.isProImageView.image = UIImage(named: K.General.isAvailableImage, in: bundle, compatibleWith: nil)
            } else {
                cell.isProImageView.image = UIImage(named: K.General.isLockedImage, in: bundle, compatibleWith: nil)
            }
            
        }
        cell.title.text = comparisonDict[index][K.General.featureTitle] as? String ?? ""
        return cell
    }
    func configureCollectionCell(_ cell: CollectionTableViewCell) -> CollectionTableViewCell {
        cell.successStories = self.successStories
        cell.mainVC = self
        cell.successStoriesCollection.reloadData()
        return cell
    }
    
    func configureFAQCell(_ cell: ExpandableTableViewCell, index: Int) -> ExpandableTableViewCell {
        let bundle = Bundle(for: type(of: self))
        let faq = faqDict[index]
        cell.questionLable.text = faq[K.General.question] as? String ?? ""
        if let expand = faq[K.General.expandable] as? Bool, expand == true {
            cell.expandableImageView.image = UIImage(named: K.CellIdentifiers.collapseImage, in: bundle, compatibleWith: nil)
            let answerText = faq[K.General.answer] as? String ?? ""
            cell.answerText = answerText
            let formattedText = String.format(strings: answerText.detectedLinks, boldColor: UIColor.blue, inString: answerText)
            cell.answerLable.attributedText = formattedText
            if answerText.detectedLinks.count > 0 {
                cell.delegate = self
                cell.addTapGesture()
            }
        } else {
            cell.expandableImageView.image = UIImage(named: K.CellIdentifiers.expandImage, in: bundle, compatibleWith: nil)
            cell.answerLable.text = ""
        }
        cell.selectionStyle = .none
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            if let expandable = faqDict[indexPath.row][K.General.expandable] as? Bool, expandable == false {
                faqDict[indexPath.row][K.General.expandable] = true
            } else {
                faqDict[indexPath.row][K.General.expandable] = false
            }
            tableView.beginUpdates()
            tableView.reloadRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
}

extension IAPViewController : FAQLinkOpening {
    func openLink(url : URL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
